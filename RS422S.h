/* 
 * File:   RS422S.h
 * Author: Shuuya
 *
 * Created on 2016/03/16, 19:59
 */

#ifndef RS422S_H
#define	RS422S_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "dsPIC33FJ128_head.h"
#include "defines_for_RS422.h"


#ifndef RS422_TX_SW
#define RS422_TX_SW _LATB5
#endif

#ifndef RS422_bps
#define RS422_bps UART1_bps
#endif

#ifndef RS422_TXREG
#define RS422_TXREG U1TXREG
#endif

#ifndef RS422_RXREG
#define RS422_RXREG U1RXREG
#endif

#ifndef RS422_TXIF
#define RS422_TXIF IFS0bits.U1TXIF
#endif

#ifndef RS422_TXIE
#define RS422_TXIE IEC0bits.U1TXIE
#endif

#ifndef RS422_RXIF
#define RS422_RXIF IFS0bits.U1RXIF
#endif  

#ifndef RS422_ADDEN
#define RS422_ADDEN U1STAbits.ADDEN
#endif  

#ifndef RS422_TMR_ON_nOFF
#define RS422_TMR_ON_nOFF T5CONbits.TON
#endif    

#ifndef RS422_TMR
#define RS422_TMR TMR5
#endif

#ifndef MAX_TMR_COUNT
#define MAX_TMR_COUNT 0xFFFF
#endif

#ifndef RS422_TMR_TCKPS
#define RS422_TMR_TCKPS T5CONbits.TCKPS
#endif      

#ifndef RS422_TMRIE
#define RS422_TMRIE IEC1bits.T5IE
#endif    

#ifndef RS422_TMRIF
#define RS422_TMRIF IFS1bits.T5IF
#endif

#ifndef MAX_DATA_SIZE
#define MAX_DATA_SIZE 32
#endif  

#ifndef MAX_DATA_KINDS
#define MAX_DATA_KINDS 32
#endif

#ifndef BUFFER_SIZE
#define BUFFER_SIZE (3+(MAX_DATA_SIZE))
#endif
    //deviceID,dataID,length,datas

    /*  マクロ関数  */
#define set_data_type(deviceID_num,dataID_num,data_type) device_id[ deviceID_num ].data_id[ dataID_num ].status.type= data_type ;
    //データ群のデータタイプを指定する関数,引数：(deviceID,dataID,指定するdataタイプ)
#define set_ACK_RS422()      {start_reply();putUART_RS422(6, UART_data_type);now_buffer_position_S = 0;}
#define set_NACK_RS422()     {start_reply();putUART_RS422(0x15, UART_data_type);now_buffer_position_S = 0;}
#define putUART_RS422(datas,typeUART) {\
    RS422_TXIF  = 0;\
    if (typeUART == UART_data_type) {\
        RS422_TXREG = (((uint16_t)datas) & 0x00FF);\
    } else if (typeUART == UART_add_type) {\
        RS422_TXREG = ((((uint16_t)datas) & 0x00FF) | 0x0100);\
    }\
}    
    
#define search_length       (data_id_S[search_dataID_S].length)      //現在参照している有効length    
#define search_dataType      (data_id_S[search_dataID_S].status.type)      //現在参照している有効dataタイプ
#define search_dataStatus    (state_RS422.now_RS422)
    
#define start_transmit() {RS422_TX_SW = 1;search_dataStatus = now_transmit;\
    RS422_TXIF   = 0;RS422_TXIE =1;RS422_ADDEN = 0;}//受信を許可 
    
#define start_reply()    {RS422_TX_SW = 1;search_dataStatus = now_reply;\
    RS422_TXIF   = 0;RS422_TXIE =1;RS422_ADDEN = 0;}//受信を許可
    
#define start_recieve()  {RS422_TX_SW = 0;search_dataStatus = now_recieve;\
    RS422_TXIF   = 0;RS422_TXIE =0;RS422_ADDEN = 0;}//受信を遮断
    
#define start_idle()     {RS422_TX_SW = 0;search_dataStatus = now_idle;\
    RS422_TXIF   = 0;RS422_TXIE =0;RS422_ADDEN = 1;}

#define init_PWM_RS422S() {\
        dutyA_percent_RS422S = 0.0;\
        dutyB_percent_RS422S = 0.0;\
}    
    
#define init_PID_RS422S() {\
        P_A_gein_RS422S=0.0;\
        I_A_gein_RS422S=0.0;\
        D_A_gein_RS422S=0.0;\
        P_B_gein_RS422S=0.0;\
        I_B_gein_RS422S=0.0;\
        D_B_gein_RS422S=0.0;\
        Drpm_A_RS422S = 0.0;\
        Drpm_B_RS422S = 0.0;\
        now_Drpm_A_RS422S =0.0;\
        now_Drpm_B_RS422S =0.0;\
        now_rpm_A_RS422S =0.0;\
        now_rpm_B_RS422S =0.0;\
}
    
#define init_Position_RS422S() {\
        POS1CNT = 0x7FFF;\
        POS2CNT = 0x7FFF;\
        P_A_gein_RS422S=0.0;\
        I_A_gein_RS422S=0.0;\
        D_A_gein_RS422S=0.0;\
        P_B_gein_RS422S=0.0;\
        I_B_gein_RS422S=0.0;\
        D_B_gein_RS422S=0.0;\
        Dposition_A_RS422S = 0.0;\
        Dposition_B_RS422S = 0.0;\
        now_position_A_RS422S = 0.0;\
        now_position_B_RS422S = 0.0;\
}
    
    typedef union {
        uint8_t byte;

        struct {
            unsigned now_RS422 : 4;
            unsigned final_reply_ToMe : 1;
            unsigned other : 4;
        };

    } data_state_RS422;
    
    typedef union {
        uint8_t byte;

        struct {
            unsigned type : 2; //(dataの型)
            unsigned CHECK : 1; //通信の状態（成功か失敗か）
            unsigned now_RS422 : 3; //現在の通信状態
            unsigned OTHER : 2;
        };

    } data_status;

    typedef struct msgstr {
        uint8_t data[MAX_DATA_SIZE]; //(有効な命令もしくはデータの配列) ※宣言時、配列[n]={};ですべて0になります（知らんかった）
        uint8_t length; //dataの配列個数
        data_status status; //(受信データ=0　or 送信データ=1 or 無効なデータ=2)
    } rs422_data; //0~F:モータコントローラ

    extern data_state_RS422 state_RS422;
    extern uint8_t search_dataID_S; //現在参照している有効dataID
    extern uint8_t now_buffer_position_S;
    extern uint16_t number_of_valid_data_S; //有効データ数 ※データが超膨大でないことを仮定(最大:short:32台,64個の32byteの配列)
    extern uint8_t buffer_422S[BUFFER_SIZE]; //返信用のバッファ
    extern uint8_t device_id_S; //デバイス固有のID
    extern uint8_t data_head_id_S; //データidの先頭の値
    extern rs422_data data_id_S[MAX_DATA_KINDS]; //データ群,MAX_DATA_KINDSをdeviceによって可変させる
    extern PID_variables elements;

    void Start_RS422(uint8_t); //初めに呼び出すべき関数(初期化関数)
    void set_reply_mes();
    void rx_ISR_RS422(); //受信割り込み関数にて呼び出すべき関数(定期通信データベース化関数)
    void tx_ISR_RS422(); //送信割り込み関数にて呼び出すべき関数（連続送信関数）
    void tmr_ISR_PID_Control_RS422();
    void PWM_generater_RS422();

#ifdef	__cplusplus
}
#endif

#endif	/* RS422S_H */

