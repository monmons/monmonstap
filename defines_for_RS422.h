/* 
 * File:   defines_for_RS422.h
 * Author: Shuuya
 *
 * Created on 2016/07/11, 18:39
 */

#ifndef DEFINES_FOR_RS422_H
#define	DEFINES_FOR_RS422_H



#ifdef	__cplusplus
extern "C" {
#endif
 
#define PWM_CONTROLLER (0)  //データ個数2個    
#define POSITION_CONTROLLER (1)  //データ個数7個
#define SERVO_CONTROLLER (2)  //データ個数1個
#define PID_CONTROLLER (3)  //データ個数7個
#define GPIO_CONTROLLER (4)  //データ個数7個



    
    
    /* 設定　*/
#define invalid_DATAtype 0
    
#define recieve_DATAtype 1
    
#define transmit_DATAtype 2
    
#define superM_recieve_DATAtype 3
    
#define superM_transmit_DATAtype 4
    
#define SUCCESSFUL_RS422 1
    
#define FAILURE_RS422 0
    
#define now_idle 0
    
#define now_transmit 1
    
#define now_recieve 2
    
#define now_reply 3
    
#define ACK_data (0x6)
    
    
    /* data_idのdefine */

#define STAP_mode_id 0    
    
#define Drpm_RS422_id    1
    
#define Dposition_RS422_id    1

#define PID_A_gein_RS422_id  2

#define PID_B_gein_RS422_id  3

#define Drpm_up_speed_RS422_id 4

#define now_Drpm_RS422_id  5
 
#define now_Dposition_RS422_id    5

#define now_rpm_RS422_id  6
    
#define now_position_RS422_id  6

#define now_duty_RS422_id  7

#define duty_servo_RS422_id    8

#define duty_percent_RS422_id    9
    
#define GPIO_RS422_id    10


    /* data長のdefine */

#define Drpm_RS422_length    8

#define PID_gein_RS422_length  24

#define Drpm_up_speed_RS422_length 4

#define now_Drpm_RS422_length  4

#define now_rpm_RS422_length  8

#define now_duty_RS422_length  8

#define duty_servo_RS422_length    8

#define duty_percent_RS422_length    8


    
    /*  data種別のdefine    */

#define Drpm_RS422S_type    recieve_DATAtype

#define PID_A_gein_RS422S_type  recieve_DATAtype

#define PID_B_gein_RS422S_type recieve_DATAtype

#define Drpm_up_speed_RS422S_type recieve_DATAtype

#define now_Drpm_RS422S_type  transmit_DATAtype

#define now_rpm_RS422S_type  transmit_DATAtype

#define now_duty_RS422S_type  transmit_DATAtype

#define duty_servo_RS422S_type   recieve_DATAtype

#define duty_percent_RS422S_type    recieve_DATAtype

    
    /*  RS422 マスター  */
    /*      dataID 割り振り     */
    /* STAP基板のモード選択 */
    
#define STAP_mode_select(device_num) (device_id[device_num].data_id[STAP_mode_id].data[0])
    
    /* PID*/
#define Drpm_A_RS422M(device_num)    ((float*)device_id[device_num].data_id[Drpm_RS422_id].data)[0]//16bit

#define Drpm_B_RS422M(device_num)    ((float*)device_id[device_num].data_id[Drpm_RS422_id].data)[1]//16bit

#define P_A_gein_RS422M(device_num)  ((float*)device_id[device_num].data_id[PID_A_gein_RS422_id].data)[0]

#define I_A_gein_RS422M(device_num) ((float*)device_id[device_num].data_id[PID_A_gein_RS422_id].data)[1]

#define D_A_gein_RS422M(device_num) ((float*)device_id[device_num].data_id[PID_A_gein_RS422_id].data)[2]

#define P_B_gein_RS422M(device_num) ((float*)device_id[device_num].data_id[PID_B_gein_RS422_id].data)[0]

#define I_B_gein_RS422M(device_num) ((float*)device_id[device_num].data_id[PID_B_gein_RS422_id].data)[1]

#define D_B_gein_RS422M(device_num) ((float*)device_id[device_num].data_id[PID_B_gein_RS422_id].data)[2]

#define Drpm_A_up_speed_RS422M(device_num) (device_id[device_num].data_id[Drpm_up_speed_RS422_id].data[0])

#define Drpm_B_up_speed_RS422M(device_num) (device_id[device_num].data_id[Drpm_up_speed_RS422_id].data[1])

#define now_Drpm_A_RS422M(device_num) ((float*)device_id[device_num].data_id[now_Drpm_RS422_id ].data)[0]//16bit

#define now_Drpm_B_RS422M(device_num) ((float*)device_id[device_num].data_id[now_Drpm_RS422_id ].data)[1]//32bit

#define now_rpm_A_RS422M(device_num) ((float*)device_id[device_num].data_id[now_rpm_RS422_id].data)[0]//32bit

#define now_rpm_B_RS422M(device_num) ((float*)device_id[device_num].data_id[now_rpm_RS422_id].data)[1]//16bit

#define now_duty_A_RS422M(device_num) ((float*)device_id[device_num].data_id[now_duty_RS422_id].data)[0]//16bit

#define now_duty_B_RS422M(device_num) ((float*)device_id[device_num].data_id[now_duty_RS422_id].data)[1]//16bit

    /* 位置制御用*/
#define Dposition_A_RS422M(device_num)   ((float*)device_id[device_num].data_id[Dposition_RS422_id].data)[0]//16bit

#define Dposition_B_RS422M(device_num)    ((float*)device_id[device_num].data_id[Dposition_RS422_id].data)[1]//16bit
    
#define now_Dposition_A_RS422M(device_num)   ((float*)device_id[device_num].data_id[now_Dposition_RS422_id].data)[0]//16bit

#define now_Dposition_B_RS422M(device_num)    ((float*)device_id[device_num].data_id[now_Dposition_RS422_id].data)[1]//16bit
    
    
    
#define now_position_A_RS422M(device_num) ((float*)device_id[device_num].data_id[now_position_RS422_id].data)[0]//32bit

#define now_position_B_RS422M(device_num) ((float*)device_id[device_num].data_id[now_position_RS422_id].data)[1]//16bit    
    
    
        /*  servo用 */

#define dutyA_servo_RS422M(device_num) ((float*)device_id[device_num].data_id[duty_servo_RS422_id].data)[0]//16bit

#define dutyB_servo_RS422M(device_num) ((float*)device_id[device_num].data_id[duty_servo_RS422_id].data)[1]//16bit

        /*  単純PWM用   */

#define dutyA_percent_RS422M(device_num) ((float*)device_id[device_num].data_id[duty_percent_RS422_id].data)[0]//16bit

#define dutyB_percent_RS422M(device_num) ((float*)device_id[device_num].data_id[duty_percent_RS422_id].data)[1]//16bit
    
    /* 単純GPIO用 */
#define GPIO_RS422M(device_num) ((uint16_t*)device_id[device_num].data_id[GPIO_RS422_id].data)[0]
    
#define restart_RS422(){\
    Start_RS422();\
}
    
#define init_GPIO_RS422(device_num){\
    device_id[device_num].data_id[STAP_mode_id].status.type = transmit_DATAtype;\
    device_id[device_num].data_id[STAP_mode_id].length = 1;\
    device_id[device_num].data_id[STAP_mode_id].data[0] = GPIO_CONTROLLER;\
    device_id[device_num].data_id[STAP_mode_id].status.Once_BidirectionalCommunication = 1;\
    \
    \
}
    
    /*マスタ,PWM設定*/
#define init_PWM_RS422(device_num){\
    device_id[device_num].data_id[STAP_mode_id].status.type = transmit_DATAtype;\
    device_id[device_num].data_id[STAP_mode_id].length = 1;\
    device_id[device_num].data_id[STAP_mode_id].data[0] = PWM_CONTROLLER;\
    device_id[device_num].data_id[STAP_mode_id].status.Once_BidirectionalCommunication = 1;\
    \
    device_id[device_num].data_id[duty_percent_RS422_id].status.type = transmit_DATAtype;\
    device_id[device_num].data_id[duty_percent_RS422_id].length = duty_percent_RS422_length;\
    device_id[device_num].data_id[duty_percent_RS422_id].status.Once_BidirectionalCommunication = 0;\
}    
    
    
    /*マスタ、position設定　 */
#define init_Position_RS422(device_num){\
    device_id[device_num].data_id[STAP_mode_id].status.type = transmit_DATAtype;\
    device_id[device_num].data_id[STAP_mode_id].length = 1;\
    device_id[device_num].data_id[STAP_mode_id].data[0] = POSITION_CONTROLLER;\
    device_id[device_num].data_id[STAP_mode_id].status.Once_BidirectionalCommunication = 1;\
    \
    device_id[device_num].data_id[Dposition_RS422_id].status.type = transmit_DATAtype;\
    device_id[device_num].data_id[PID_A_gein_RS422_id].status.type = transmit_DATAtype;\
    device_id[device_num].data_id[PID_B_gein_RS422_id].status.type = transmit_DATAtype;\
    device_id[device_num].data_id[now_position_RS422_id].status.type = recieve_DATAtype;\
    device_id[device_num].data_id[now_duty_RS422_id].status.type = recieve_DATAtype;\
    \
    device_id[device_num].data_id[Dposition_RS422_id].length = 8;\
    device_id[device_num].data_id[PID_A_gein_RS422_id].length = 12;\
    device_id[device_num].data_id[PID_B_gein_RS422_id].length = 12;\
    device_id[device_num].data_id[now_position_RS422_id].length = 8;\
    device_id[device_num].data_id[now_duty_RS422_id].length = 8;\
    \
    device_id[device_num].data_id[PID_A_gein_RS422_id].status.Once_BidirectionalCommunication = 1;\
    device_id[device_num].data_id[PID_B_gein_RS422_id].status.Once_BidirectionalCommunication = 1;\
}
    
    
    /*  RS422 スレーブ*/
    /*      dataID 割り振り     */

#define STAP_mode_select_RS422S (data_id_S[STAP_mode_id].data[0])    
    
#define Drpm_A_RS422S    ((float*)data_id_S[Drpm_RS422_id].data)[0]//16bit

#define Drpm_B_RS422S    ((float*)data_id_S[Drpm_RS422_id].data)[1]//16bit

#define P_A_gein_RS422S  ((float*)data_id_S[PID_A_gein_RS422_id].data)[0]

#define I_A_gein_RS422S ((float*)data_id_S[PID_A_gein_RS422_id].data)[1]

#define D_A_gein_RS422S ((float*)data_id_S[PID_A_gein_RS422_id].data)[2]

#define P_B_gein_RS422S ((float*)data_id_S[PID_B_gein_RS422_id].data)[0]

#define I_B_gein_RS422S ((float*)data_id_S[PID_B_gein_RS422_id].data)[1]

#define D_B_gein_RS422S ((float*)data_id_S[PID_B_gein_RS422_id].data)[2]

#define Drpm_A_up_speed_RS422S (((int8_t*)data_id_S[Drpm_up_speed_RS422_id].data)[0])

#define Drpm_B_up_speed_RS422S (((int8_t*)data_id_S[Drpm_up_speed_RS422_id].data)[1])

#define now_Drpm_A_RS422S ((float*)data_id_S[now_Drpm_RS422_id ].data)[0]//16bit

#define now_Drpm_B_RS422S ((float*)data_id_S[now_Drpm_RS422_id ].data)[1]//32bit

#define now_rpm_A_RS422S ((float*)data_id_S[now_rpm_RS422_id].data)[0]//32bit

#define now_rpm_B_RS422S ((float*)data_id_S[now_rpm_RS422_id].data)[1]//16bit

#define now_duty_A_RS422S ((float*)data_id_S[now_duty_RS422_id].data)[0]//16bit

#define now_duty_B_RS422S ((float*)data_id_S[now_duty_RS422_id].data)[1]//16bit

    /* 位置制御用*/
#define Dposition_A_RS422S    ((float*)data_id_S[Dposition_RS422_id].data)[0]//32bit

#define Dposition_B_RS422S   ((float*)data_id_S[Dposition_RS422_id].data)[1]//32bit
    
#define now_Dposition_A_RS422S    ((float*)data_id_S[now_Dposition_RS422_id].data)[0]//32bit

#define now_Dposition_B_RS422S   ((float*)data_id_S[now_Dposition_RS422_id].data)[1]//32bit
    
#define now_position_A_RS422S ((float*)data_id_S[now_position_RS422_id].data)[0]//32bit

#define now_position_B_RS422S ((float*)data_id_S[now_position_RS422_id].data)[1]//16bit    
    
    
        /*  servo用 */

#define dutyA_servo_RS422S ((uint16_t*)data_id_S[duty_servo_RS422_id].data)[0]//16bit

#define dutyB_servo_RS422S ((uint16_t*)data_id_S[duty_servo_RS422_id].data)[1]//16bit

        /*  単純PWM用   */

#define dutyA_percent_RS422S   ((float*)data_id_S[duty_percent_RS422_id].data)[0]//16bit

#define dutyB_percent_RS422S   ((float*)data_id_S[duty_percent_RS422_id].data)[1]//16bit
    
        /* 単純GPIO用 */
#define GPIO_RS422S(device_num) ((uint16_t*)data_id_S[GPIO_RS422_id].data)[0]    
    
    /* superRS422*/
#define API_StartData (0x7E)
    
    
    
#ifdef	__cplusplus
}
#endif

#endif	/* DEFINES_FOR_RS422_H */

