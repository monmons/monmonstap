/* Microchip Technology Inc. and its subsidiaries.  You may use this software 
 * and any derivatives exclusively with Microchip products. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION 
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE 
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS 
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF 
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE 
 * TERMS. 
 */

/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef DEFINES_FOR_DSPIC_H
#define	DEFINES_FOR_DSPIC_H


#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    
    
#ifndef Fcyc
#define Fcyc (40000000.0)//NbNüg
#endif
    
    /**/
#ifndef nano
#define nano  (1.0/1000000000.0)//PÊn
#endif

#ifndef micro    
#define micro (1.0/1000000.0)//PÊn
#endif
    
#ifndef milli
#define milli (1.0/1000.0)  //PÊn
#endif
    
#ifndef	ON
#define ON 1
#endif
    
#ifndef	OFF
#define OFF 0
#endif
    
#ifndef	active_HI
#define active_HI 1
#endif
    
#ifndef	active_LOW
#define active_LOW 0
#endif
    
#ifndef	GO_mode_mecanum
#define GO_mode_mecanum 0
#endif
    
#ifndef	TURN_mode_mecanum
#define TURN_mode_mecanum 0
#endif
    
#ifndef eight_bitsUARTtype
#define eight_bitsUARTtype 0
#endif

#ifndef nine_bitsUARTtype
#define nine_bitsUARTtype 1
#endif
    
#ifndef UART_data_type
#define UART_data_type 0
#endif  

#ifndef UART_add_type
#define UART_add_type 1
#endif    
    
#define position_control 0

#define speed_control 1


    
    /*pin*/

#ifndef	not_used_module
#define not_used_module 200
#endif    

#ifndef	RP0
#define RP0 0
#endif
    
#ifndef	RP1
#define RP1 1
#endif
    
#ifndef	RP2
#define RP2 2
#endif
    
#ifndef	RP3
#define RP3 3
#endif
    
#ifndef	RP4
#define RP4 4
#endif
    
#ifndef	RP5
#define RP5 5
#endif
    
#ifndef	RP6
#define RP6 6
#endif
    
#ifndef	RP7
#define RP7 7
#endif
    
#ifndef	RP8
#define RP8 8
#endif
    
#ifndef	RP9
#define RP9 9
#endif
    
#ifndef	RP10
#define RP10 10
#endif
    
#ifndef	RP11
#define RP11 11
#endif
    
#ifndef	RP12
#define RP12 12
#endif
    
#ifndef	RP13
#define RP13 13
#endif
    
#ifndef	RP14
#define RP14 14
#endif
    
#ifndef	RP15
#define RP15 15
#endif
    
    /* PIDp */
#define DPA_PID ((*(elements->P_A_gein)) * (ErrorA1))
    
#define DIA_PID ((*(elements->I_A_gein)) * (ErrorA_total))

#define DDA_PID ((*(elements->D_A_gein)) * (Error_differential_A))
 
    
#define DPB_PID ((*(elements->P_B_gein)) * (ErrorB1))
    
#define DIB_PID ((*(elements->I_B_gein)) * (ErrorB_total))

#define DDB_PID ((*(elements->D_B_gein)) * (Error_differential_B))
 
    
    
    
#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif

