/* 
 * File:   dsPIC33FJ128_head.h
 * Author: Shuuya
 *
 * Created on 2016/03/21, 22:26
 */

#ifndef DSPIC33FJ128_HEAD_H
#define	DSPIC33FJ128_HEAD_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <xc.h>    
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <libpic30.h>
#include <stdbool.h>
#include <math.h>
#include "defines_for_dsPIC.h"
    
    /*UART*/

#ifndef UART1_bps
#define UART1_bps (115200.0)//UART1のボーレート
#endif  

#ifndef UART2_bps
#define UART2_bps (115200.0)//UART2のボーレート
#endif  
    /* eight_bitsUARTtype:8bit通信  nine_bitsUARTtype:9bit通信 */
#ifndef UART1_type
#define UART1_type nine_bitsUARTtype//8bitもしくは9bitの通信
#endif

#ifndef UART2_type
#define UART2_type eight_bitsUARTtype//8bitもしくは9bitの通信
#endif  

    /* I2C */
#define Fscl (400000.0)//I2C周波数
#define I2C_BAUD (((((1/Fscl) - (130.0/1000000000.0))*((float)Fcyc)))-2)

    /*OutputCompare*/

#ifndef OC_1and2_frequency
#define OC_1and2_frequency (10000.0)//OC1,2のPWM周波数(単位:Hz,小数点をつける),TMR2をタイマーベースとしている
#endif  

#ifndef OC_1and2_period
#define OC_1and2_period (( (1.0) / (OC_1and2_frequency)) * (1000000.0))//OC1,2のPWM周波数(単位:us,小数点をつける),TMR2をタイマーベースとしている
#endif  

#ifndef OC_3and4_frequency
#define OC_3and4_frequency (10000.0)//OC3,4のPWM周波数(単位:Hz,小数点をつける),TMR3をタイマーベースとしている
#endif      

#ifndef OC_3and4_period
#define OC_3and4_period (((1.0)/(OC_3and4_frequency)) * (1000000.0))//OC3,4のPWM周期(単位:us,小数点をつける),TMR3をタイマーベースとしている
#endif  

    /*Moter_OutputCompare (1,2と3,4のOCは同じタイマーピリオド)*/

#ifndef OC_for_MOTER_A
#define OC_for_MOTER_A 1//MOTER_Aに使うOCの番号
#endif 

#ifndef OC_for_MOTER_B
#define OC_for_MOTER_B 2//MOTER_Bに使うOCの番号
#endif 

#ifndef LAT_for_MOTER_A1
#define LAT_for_MOTER_A1 (LATBbits.LATB14)//モータの正転にて使うポート
#endif  

#ifndef LAT_for_MOTER_A2
#define LAT_for_MOTER_A2 (LATBbits.LATB15)//モータの逆転にて使うポート
#endif 

#ifndef LAT_for_MOTER_B1
#define LAT_for_MOTER_B1 (LATBbits.LATB12)//モータの正転にて使うポート
#endif 

#ifndef LAT_for_MOTER_B2
#define LAT_for_MOTER_B2 (LATBbits.LATB13)//モータの逆転にて使うポート
#endif 

    /*QEI*/

#ifndef QEI1_type
#define QEI1_type speed_control//位置制御もしくは速度制御
#endif

#ifndef QEI2_type
#define QEI2_type speed_control//位置制御もしくは速度制御
#endif    

#define QEI_sampling_MilliSec (10.0)//サンプリング時間(ms),使用しないときは0としてください
#define QEI_sampling_Sec      ((QEI_sampling_MilliSec) / (1000.0))//サンプリング時間(s)
#define QEI_sampling_Minute ((QEI_sampling_Sec) / (60.0))//サンプリング時間(min)
    
#define Rotary_encoder1_pulse_per_revolution   (300.0)//ロータリーエンコーダ1の1回転当たりのパルス数
#define QEI1_pulse_per_revolution              ((4.0) *(Rotary_encoder1_pulse_per_revolution))//QEIにて4倍モードの使用を前提としている
#define QEI1_revolution_per_pulse              ((1.0) / ((4.0) *(Rotary_encoder1_pulse_per_revolution)))//QEIにて4倍モードの使用を前提としている
    
#define Rotary_encoder2_pulse_per_revolution   (300.0)//ロータリーエンコーダ2の1回転当たりのパルス数
#define QEI2_pulse_per_revolution              ((4.0) *(Rotary_encoder2_pulse_per_revolution))//QEIにて4倍モードの使用を前提としている
#define QEI2_revolution_per_pulse              ((1.0) / ((4.0) *(Rotary_encoder2_pulse_per_revolution)))//QEIにて4倍モードの使用を前提としている


    /*各モジュール ピン設定(RBxの形式で書いてください) モジュール自体を使用しない場合 not_used_module としてください*/

    /*OutputCompare*/
#ifndef OC1_PIN
#define OC1_PIN RP10
#endif 

#ifndef OC2_PIN
#define OC2_PIN RP11
#endif 

#ifndef OC3_PIN
#define OC3_PIN not_used_module
#endif 

#ifndef OC4_PIN
#define OC4_PIN not_used_module
#endif 

    /*UART*/
#ifndef UART1_PIN_TX
#define UART1_PIN_TX RP3
#endif 

#ifndef UART1_PIN_RX
#define UART1_PIN_RX RP2
#endif 

#ifndef UART2_PIN_TX
#define UART2_PIN_TX not_used_module//RP9
#endif 

#ifndef UART2_PIN_RX
#define UART2_PIN_RX not_used_module//RP8
#endif 

    /*QEI*/
#ifndef QEI1_PIN_A
#define QEI1_PIN_A RP6
#endif  

#ifndef QEI1_PIN_B
#define QEI1_PIN_B RP7
#endif     

#ifndef QEI2_PIN_A
#define QEI2_PIN_A RP8
#endif     

#ifndef QEI2_PIN_B
#define QEI2_PIN_B RP9
#endif  

    /*各モジュールの割り込み優先度(割り込みを使用しない場合 0　と書いてください , 0~7) モジュール自体を使用しない場合 not_used_moduleとしてください*/

    /*UART*/
#ifndef UART1TX_IP
#define UART1TX_IP 5
#endif 

#ifndef UART1RX_IP
#define UART1RX_IP 5
#endif 

#ifndef UART2TX_IP
#define UART2TX_IP 0
#endif     

#ifndef UART2RX_IP
#define UART2RX_IP 0
#endif     

    /*TMR*/
#ifndef TMR1_IP
#define TMR1_IP 1
#endif     

#ifndef TMR2_IP
#define TMR2_IP 0
#endif     

#ifndef TMR3_IP
#define TMR3_IP 0
#endif     

#ifndef TMR4_IP
#define TMR4_IP 0
#endif     

#ifndef TMR5_IP
#define TMR5_IP 0
#endif 

    /*QEI*/
#ifndef QEI1_IP
#define QEI1_IP 0
#endif     

#ifndef QEI2_IP
#define QEI2_IP 0
#endif  

    /*I2C(EEPROM用)*/
#ifndef I2CM_IP
#define I2CM_IP 0
#endif      

    /*以下は変更しなくてよい*/

    /*マクロ関数*/
    void setup_ports();
    
#define setup_dsPIC() {OSCTUN = 18;\
    CLKDIVbits.FRCDIV = 0;\
    CLKDIVbits.PLLPRE = 0;\
    PLLFBD = 40 - 2;\
    CLKDIVbits.PLLPOST = 0;\
    RCONbits.SWDTEN = 0;\
    __builtin_write_OSCCONH(0x01);\
    __builtin_write_OSCCONL(0x01);\
    while (OSCCONbits.COSC != 0b001);\
    while (OSCCONbits.LOCK == 0);\
    AD1PCFGL = 0xFFFF;TRISB=0xFFFF;\
    CORCONbits.IPL3 = 0;\
    SRbits.IPL = 0;\
    INTCON1bits.NSTDIS = 0;\
    INTCON2bits.ALTIVT = 0;\
    setup_ports();\
    TRISBbits.TRISB5  = 0;\
    LATBbits.LATB5  = 0;\
    TRISB = TRISB & 0x0FFF;\
}    //40MHz,WDTの無効,main関数にて最初に呼び出してください!

#define setupI2C_M(){PMD1bits.I2C1MD=0;\
    TRISBbits.TRISB8 = 1;\
    TRISBbits.TRISB9 = 1;\
    I2C1CON=0;\
    I2C1BRG=I2C_BAUD;\
    I2C1MSK=0;\
    I2C1CONbits.I2CEN=1;\
    I2C1ADD=0;\
    IPC1bits.IC2IP=I2CM_IP;\
    IEC1bits.MI2C1IE=1;\
    __delay_ms(1);}

#define putI2C_M (datas) {IFS1bits.MI2C1IF=0; I2C1TRN = datas;}    

#define putUART1(datas) {IFS0bits.U1TXIF = 0; U1TXREG = datas;}
#define putUART2(datas) {IFS1bits.U2TXIF = 0; U2TXREG = datas;}

#define SET_TRUN_NORMAL_A()     {LAT_for_MOTER_A1=0;LAT_for_MOTER_A2=1;}
#define SET_TRUN_REVERSE_A()    {LAT_for_MOTER_A1=1;LAT_for_MOTER_A2=0;}
#define SET_BRAKE_A()           {LAT_for_MOTER_A1=0;LAT_for_MOTER_A2=0;}

#define SET_TRUN_NORMAL_B()     {LAT_for_MOTER_B1=0;LAT_for_MOTER_B2=1;}
#define SET_TRUN_REVERSE_B()    {LAT_for_MOTER_B1=1;LAT_for_MOTER_B2=0;}
#define SET_BRAKE_B()           {LAT_for_MOTER_B1=0;LAT_for_MOTER_B2=0;}    

    typedef union {
        uint8_t byte;

        struct {
            unsigned WasteRecieve : 1; //次の受信データを破棄するかどうか
            unsigned FirstRXIF : 1; //次の受信データが１番目のデータかどうか
            unsigned useful : 1; //受信データ群が有効かどうか
            unsigned recievetime : 5; //受信回数
        };

    } Controler_status;

    typedef struct PID_var {
        int16_t* DrpmA;
        int16_t* DrpmB;

        float* P_A_gein;
        float* I_A_gein;
        float* D_A_gein;

        float* P_B_gein;
        float* I_B_gein;
        float* D_B_gein;

        int8_t* DrpmA_up_speed;
        int8_t* DrpmB_up_speed;

        float* now_DrpmA;
        float* now_DrpmB;

        float* now_rpmA1;
        float* now_rpmB1;

        float* now_dutyA;
        float* now_dutyB;

    } PID_variables;
    
    typedef struct Position_var {
        float* DpositionA;//回転　回数
        float* DpositionB;//回転　回数
        
        float* now_DpositionA;//回転　回数
        float* now_DpositionB;//回転　回数

        float* P_A_gein;
        float* I_A_gein;
        float* D_A_gein;
        
        float* P_B_gein;
        float* I_B_gein;
        float* D_B_gein;
        
        float* dutyA;
        float* dutyB;
        
        float* now_positionA;
        float* now_positionB;

    } Position_variables;
    

    extern uint16_t T2CKPS;
    extern uint16_t T3CKPS;
    extern uint8_t str[];
    extern float QEICNT1; 

    

    void setupUART(void);
    void I2Cwrite_M();
    void setupOC(void); //PWM周期(単位:ms)を引数とする,TMR2をタイマーベースとしている
    void setdutySec_for_OC(unsigned char, bool, unsigned short); //(動かすOCの番号,アクティブハイかどうか,パルス幅(μs))
    void setduty_OC(uint8_t, bool, uint16_t); //(動かすOCの番号,アクティブハイかどうか,duty値):::dutyの絶対値をPR2より小さくする

    void setupTMR(void);
    void setdelay(unsigned char, unsigned char, float); //単位:μs,(モード,作りたい時間)モード1:milliSec,モード2:microSec,モード3:nanoSec  **注意**短すぎたり,長すぎるとオーバーフローします

    void setupQEI(void);

    bool Controler_checker_ISR(unsigned char, unsigned char*); //確認の合否を返り値とする(チェック動作の変化,モード２で必要なポインタ)    モード１:割り込みなし時の'S'２回を待つ動作,モード２:コントローラからのデータを確認する
    unsigned char makeASCII(unsigned char); //0~9までの10進数をASCIIコードに変える
    void makeASCIIs(unsigned const char, int16_t, uint8_t*); //signed short型の変数を,分割,ASCIIコードへの変換を行い,char型のポインタに入れる,(１番目の変数が進数の値,変換したい変数,変換後のデータを入れるポインタ型(要素数：7))
    void tmr_ISR_PID_Control(PID_variables*);
    void tmr_ISR_Position_Control(Position_variables*);
    
    
#ifdef	__cplusplus
}
#endif

#endif	/* DSPIC33FJ128_HEAD_H */

