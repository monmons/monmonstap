#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <p33FJ128MC802.h>  
#include "RS422S.h"

data_state_RS422 state_RS422 = {};
uint8_t search_dataID_S = 0; //現在参照している有効dataID
uint8_t now_buffer_position_S = 0;
uint16_t number_of_valid_data_S = 0; //有効データ数 ※データが超膨大でないことを仮定(最大:short:32台,64個の32byteの配列)
uint8_t buffer_422S[BUFFER_SIZE] = {}; //返信用のバッファ
uint8_t device_id_S = 0; //デバイス固有のID
uint8_t data_head_id_S = 0; //データidの先頭の値
rs422_data data_id_S[MAX_DATA_KINDS] = {}; //データ群,MAX_DATA_KINDSをdeviceによって可変させる
PID_variables elements;
Position_variables elements_Position;

void Start_RS422(uint8_t Device_type) {
    RS422_TX_SW = 0;
    data_head_id_S = Device_type;

    if (Device_type == PID_CONTROLLER) {

        TRISA = 0x0F;
        number_of_valid_data_S = 7;
        /*  データ種別の設定    */
        data_id_S[Drpm_RS422_id].status.type = Drpm_RS422S_type;
        data_id_S[PID_A_gein_RS422_id].status.type = PID_A_gein_RS422S_type;
        data_id_S[PID_B_gein_RS422_id].status.type = PID_B_gein_RS422S_type;
        data_id_S[Drpm_up_speed_RS422_id].status.type = Drpm_up_speed_RS422S_type;
        data_id_S[now_Drpm_RS422_id].status.type = now_Drpm_RS422S_type;
        data_id_S[now_rpm_RS422_id].status.type = now_rpm_RS422S_type;
        data_id_S[now_duty_RS422_id].status.type = now_duty_RS422S_type;


        device_id_S = (PORTA & 0x0F); //2進数4桁によるユニークIDを設定する

    } else if (Device_type == SERVO_CONTROLLER) {
        TRISA = 0x0F;
        number_of_valid_data_S = 1;

        /*  データ種別の設定    */
        data_id_S[duty_servo_RS422_id].status.type = duty_servo_RS422S_type;

        device_id_S = (PORTA & 0x0F); //2進数4桁によるユニークIDを設定する
    } else if (Device_type == PWM_CONTROLLER) {

        TRISA = 0x0F;
        number_of_valid_data_S = 1;

        /*  データ種別の設定    */
        data_id_S[duty_percent_RS422_id].status.type = duty_percent_RS422S_type;


        device_id_S = (PORTA & 0x0F); //2進数4桁によるユニークIDを設定する
    }

    start_idle();
    return;
} //初めに呼び出すべき関数(初期化関数)

void set_reply_mes() {

    buffer_422S[0] = device_id_S;
    uint8_t i;
    for (i = 0; i < search_length; i++) {
        buffer_422S[i + 1] = data_id_S[search_dataID_S].data[i];
    }

    start_transmit();
    putUART_RS422(buffer_422S[0], eight_bitsUARTtype);
    now_buffer_position_S = 1;
    return;

}

void rx_ISR_RS422() {
    static uint8_t recieve_times_rs422 = 0; //受信回数カウント
    uint16_t now_recieve_data = 0;

    RS422_RXIF = 0;

    //UARTエラー処理
    if (U1STAbits.OERR == 1) {
        U1MODEbits.UARTEN = 0;
        U1MODEbits.UARTEN = 1;
        return;
    }

    now_recieve_data = RS422_RXREG;

    if (now_recieve_data & 0x100) {
        recieve_times_rs422 = 0;
        search_dataStatus = now_idle;
    }

    now_recieve_data = now_recieve_data & 0xFF;

    putUART2(now_recieve_data);


    if ((recieve_times_rs422 == 0) && (search_dataStatus == now_idle)) {

        if (now_recieve_data == device_id_S) {
            start_recieve();
        }//IDの一致を確認//device_idの不一致
        else {
            start_idle();
            recieve_times_rs422 = 0;
            now_recieve_data = 0;
            return;
        }//device_idの不一致
    }//1回目の受信である
        /*******************************/
    else if (search_dataStatus == now_recieve) {
        if (recieve_times_rs422 == 1) {
            if (now_recieve_data < MAX_DATA_KINDS) {
                start_recieve();
                //search_dataID_S = ((now_recieve_data & 0xFF) - data_head_id_S); //現在参照している有効dataID
                search_dataID_S = (now_recieve_data & 0xFF);
                RS422_ADDEN = 0;
            }//dataIDの一致を確認
            else {
                start_reply();
                RS422_ADDEN = 1; //エラー終了
                set_NACK_RS422();
            }
        }//2回目の受信である
            /*******************************/
        else if ((recieve_times_rs422) == 2) {

            search_length = (now_recieve_data & 0x7F);
            if ((now_recieve_data & 0x80) != 0) {
                search_dataType = recieve_DATAtype;
                start_recieve();
            } else {
                search_dataType = transmit_DATAtype;
                set_reply_mes(); //データ送信の準備
            }

        }//3回目の受信である
            /**************************************/
        else {

            if ((recieve_times_rs422 == 3) && (search_dataType == transmit_DATAtype)) {

                if (now_recieve_data == ACK_data) {
                    start_idle();
                    recieve_times_rs422 = 0;
                    data_id_S[search_dataID_S].status.CHECK = SUCCESSFUL_RS422;
                    return;
                } else {
                    //set_NACK_RS422(); //未定
                }//ACK以外
            }

            start_recieve();
            data_id_S[search_dataID_S].data[(recieve_times_rs422 - 3)] = now_recieve_data; //受信データを格納する

            if (recieve_times_rs422 == (search_length + 2)) {
                set_ACK_RS422(); //"ACK"の返信
                recieve_times_rs422 = 0;
                return;
            }//length個分の受信をしたら"ACK"の返信

        }//4回目以降の受信である
    }


    //start_recieve() //受信の開始
    recieve_times_rs422++;
    return;

} //受信割り込み関数にて呼び出すべき関数(定期通信データベース化関数)

void tx_ISR_RS422() {

    RS422_TXIF = 0;

    if (((now_buffer_position_S == 0) && (search_dataStatus == now_idle))
            || (search_dataStatus == now_reply)) {
        start_idle();
        return;
    }//送信データの誤送の可能性防止,0番目のデータは必ずこの関数以外で送信される,ACKの送信の場合 など

    if ((now_buffer_position_S == (search_length + 1)) && (search_dataType == (transmit_DATAtype))) {
        //送信データで、length分の送信が終わったとき
        now_buffer_position_S = 0;
        start_recieve() //受信の開始
        return;
    }
    start_transmit();
    putUART_RS422(buffer_422S[now_buffer_position_S], eight_bitsUARTtype); //次のデータを送信準備

    now_buffer_position_S++;
    return;

} //送信割り込み関数にて呼び出すべき関数（連続送信関数）

#if 0
void tmr_ISR_PID_Control_RS422(void) {

    elements.P_A_gein = &(P_A_gein_RS422S);
    elements.I_A_gein = &(I_A_gein_RS422S);
    elements.D_A_gein = &(D_A_gein_RS422S);
    elements.P_B_gein = &(P_B_gein_RS422S);
    elements.I_B_gein = &(I_B_gein_RS422S);
    elements.D_B_gein = &(D_B_gein_RS422S);

    elements.DrpmA = &(Drpm_A_RS422S);
    elements.DrpmB = &(Drpm_B_RS422S);

    elements.DrpmA_up_speed = &(Drpm_A_up_speed_RS422S);
    elements.DrpmB_up_speed = &(Drpm_B_up_speed_RS422S);

    elements.now_DrpmA = &(now_Drpm_A_RS422S);
    elements.now_DrpmB = &(now_Drpm_B_RS422S);

    elements.now_dutyA = &(now_duty_A_RS422S);
    elements.now_dutyB = &(now_duty_B_RS422S);

    elements.now_rpmA1 = &(now_rpm_A_RS422S);
    elements.now_rpmB1 = &(now_rpm_B_RS422S);

    tmr_ISR_PID_Control(&(elements));

    return;
}
#endif

 void tmr_ISR_Position_Control_RS422(void) {

        elements_Position.P_A_gein = &(P_A_gein_RS422S);
        elements_Position.I_A_gein = &(I_A_gein_RS422S);
        elements_Position.D_A_gein = &(D_A_gein_RS422S);
        
        elements_Position.P_B_gein = &(P_B_gein_RS422S);
        elements_Position.I_B_gein = &(I_B_gein_RS422S);
        elements_Position.D_B_gein = &(D_B_gein_RS422S);

        elements_Position.DpositionA = &(Dposition_A_RS422S);
        elements_Position.DpositionB = &(Dposition_B_RS422S);
        
        elements_Position.now_DpositionA = &(now_Dposition_A_RS422S);
        elements_Position.now_DpositionB = &(now_Dposition_B_RS422S);

        elements_Position.dutyA = &(now_duty_A_RS422S);
        elements_Position.dutyB = &(now_duty_B_RS422S);

        elements_Position.now_positionA = &(now_position_A_RS422S);
        elements_Position.now_positionB = &(now_position_B_RS422S);

    tmr_ISR_Position_Control(&(elements_Position));

    return;
}

void PWM_generater_RS422() {

    int32_t dutyA = 0;
    int32_t dutyB = 0;
    dutyA = fabs(((float) PR2) * (fabs(dutyA_percent_RS422S)));
    dutyB = fabs(((float) PR2) * (fabs(dutyB_percent_RS422S)));

    setduty_OC(OC_for_MOTER_A, active_LOW, dutyA);
    setduty_OC(OC_for_MOTER_B, active_LOW, dutyB);
    
    return;
}
