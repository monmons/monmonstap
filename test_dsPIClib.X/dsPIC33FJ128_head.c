#define FCY (20000000.0)

#include <xc.h>    
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <libpic30.h>
#include <stdbool.h>
#include <math.h>

#include "dsPIC33FJ128_head.h"

uint16_t T2CKPS = 0;
uint16_t T3CKPS = 0;
uint8_t str[7] = "000 rpm";

void setup_ports() {

    uint8_t* pointer = &(((uint8_t*) & RPOR0)[0]);
    uint16_t process = 0;

    if ((UART1_bps > 0)&&(UART1_PIN_TX != not_used_module)&&(UART1_PIN_RX != not_used_module)) {
        TRISB = (TRISB & ((1 << UART1_PIN_TX) ^ 0xFFFF)); //ピン設定
        TRISB = (TRISB | (1 << UART1_PIN_RX));
        RPINR18bits.U1RXR = UART1_PIN_RX;
        pointer[UART1_PIN_TX] = 3;
        process = process | (1 << UART1_PIN_RX);
    }

    if ((UART2_bps > 0) && (UART2_PIN_TX != not_used_module) && (UART2_PIN_RX != not_used_module)) {
        TRISB = (TRISB & ((1 << UART2_PIN_TX) ^ 0xFFFF)); //ピン設定
        TRISB = (TRISB | (1 << UART2_PIN_RX));
        RPINR19bits.U2RXR = UART2_PIN_RX;
        pointer[UART2_PIN_TX] = 5;
        process = process | (1 << UART2_PIN_RX);
    }

    if ((OC_1and2_period > 0) && (OC1_PIN != not_used_module)) {
        TRISB = (TRISB & ((1 << OC1_PIN) ^ 0xFFFF)); //ピン設定
        pointer[OC1_PIN] = 0x12;
    }

    if ((OC_1and2_period > 0) && (OC2_PIN != not_used_module)) {
        TRISB = (TRISB & ((1 << OC2_PIN) ^ 0xFFFF)); //ピン設定
        pointer[OC2_PIN] = 0x13; //ピン設定
    }

    if ((OC_3and4_period > 0) && (OC3_PIN != not_used_module)) {
        TRISB = (TRISB & ((1 << OC3_PIN) ^ 0xFFFF)); //ピン設定
        pointer[OC3_PIN] = 0x14; //ピン設定 
    }

    if ((OC_3and4_period > 0) && (OC4_PIN != not_used_module)) {
        TRISB = (TRISB & ((1 << OC4_PIN) ^ 0xFFFF)); //ピン設定
        pointer[OC4_PIN] = 0x15; //ピン設定
    }

    if ((QEI1_PIN_A != not_used_module) && (QEI1_PIN_B != not_used_module)) {
        TRISB = (TRISB | (1 << QEI1_PIN_A));
        RPINR14bits.QEA1R = QEI1_PIN_A;
        process = process | (1 << QEI1_PIN_A);
        TRISB = (TRISB | (1 << QEI1_PIN_B));
        RPINR14bits.QEB1R = QEI1_PIN_B;
        process = process | (1 << QEI1_PIN_B);
    }

    if ((QEI2_PIN_A != not_used_module) && (QEI2_PIN_B != not_used_module)) {
        TRISB = (TRISB | (1 << QEI2_PIN_A));
        RPINR16bits.QEA2R = QEI2_PIN_A;
        process = process | (1 << QEI2_PIN_A);
        TRISB = (TRISB | (1 << QEI2_PIN_B));
        RPINR16bits.QEB2R = QEI2_PIN_B;
        process = process | (1 << QEI2_PIN_B);
    }

    TRISB = TRISB & process;

    return;
}

void setupUART(void) {

    if ((UART1_bps > 0)&&(UART1_PIN_TX != not_used_module)&&(UART1_PIN_RX != not_used_module)) {

        U1MODE = 0;
        U1STA = 0;
        U1BRG = 0;

        U1MODEbits.BRGH = 1;
        PMD1bits.U1MD = 0;
        U1BRG = (((Fcyc) / (UART1_bps * 4)) - 1);

        if (UART1_type == nine_bitsUARTtype) {
            U1MODEbits.PDSEL = 3; //9bit
        } else {
            U1MODEbits.PDSEL = 0;
        }
        U1MODEbits.STSEL = 0;

        if (UART1TX_IP == 0) {
            IEC0bits.U1TXIE = 0;
            IPC3bits.U1TXIP = 0;
            IFS0bits.U1TXIF = 0;
        } else if (UART1TX_IP < 7) {
            IEC0bits.U1TXIE = 1;
            IPC3bits.U1TXIP = UART1TX_IP;
        } else {
            IEC0bits.U1TXIE = 1;
            IPC3bits.U1TXIP = 7;
        }

        if (UART1RX_IP == 0) {
            IEC0bits.U1RXIE = 0;
            IPC2bits.U1RXIP = 0;
            IFS0bits.U1RXIF = 0;
        } else if (UART1RX_IP < 7) {
            IEC0bits.U1RXIE = 1;
            IPC2bits.U1RXIP = UART1RX_IP;
            IFS0bits.U1RXIF = 0;
        } else {
            IEC0bits.U1RXIE = 1;
            IPC2bits.U1RXIP = 7;
            IFS0bits.U1RXIF = 0;
        }

        U1MODEbits.RTSMD = 0;
        U1STAbits.UTXISEL0 = 1;
        U1STAbits.UTXISEL1 = 0;

        U1MODEbits.UARTEN = 1;
        U1STAbits.UTXEN = 1;
        U1MODEbits.UEN = 0; //TX1,RX1


    }
    if ((UART2_bps > 0) && (UART2_PIN_TX != not_used_module) && (UART2_PIN_RX != not_used_module)) {

        U2MODE = 0;
        U2STA = 0;
        U2BRG = 0;

        U2MODEbits.BRGH = 1; //1:高速モード
        PMD1bits.U2MD = 0;
        U2BRG = (((Fcyc) / (UART2_bps * 4)) - 1);
        if (UART2_type == nine_bitsUARTtype) {
            U2MODEbits.PDSEL = 3; //9bit
        } else {
            U2MODEbits.PDSEL = 0;
        }
        U2MODEbits.STSEL = 0;

        if (UART2TX_IP == 0) {
            IEC1bits.U2TXIE = 0;
            IPC7bits.U2TXIP = 0;
            IFS1bits.U2TXIF = 0;
        } else if (UART2TX_IP < 7) {
            IEC1bits.U2TXIE = 1;
            IPC7bits.U2TXIP = UART2TX_IP;
            IFS1bits.U2TXIF = 0;
        } else {
            IEC1bits.U2TXIE = 1;
            IPC7bits.U2TXIP = 7;
            IFS1bits.U2TXIF = 0;
        }

        if (UART2RX_IP == 0) {
            IEC1bits.U2RXIE = 0;
            IPC7bits.U2RXIP = 0;
            IFS1bits.U2RXIF = 0;
        } else if (UART2RX_IP < 7) {
            IEC1bits.U2RXIE = 1;
            IPC7bits.U2RXIP = UART2RX_IP;
            IFS1bits.U2RXIF = 0;
        } else {
            IEC1bits.U2RXIE = 1;
            IPC7bits.U2RXIP = 7;
            IFS1bits.U2RXIF = 0;
        }

        U2MODEbits.RTSMD = 0;
        U2STAbits.UTXISEL0 = 0;
        U2STAbits.UTXISEL1 = 0;

        U2MODEbits.UARTEN = 1;
        U2STAbits.UTXEN = 1;
        U2MODEbits.UEN = 0; //TX2,RX2

    }
    __delay_ms(1);
    return;
}

void setupOC(void) {


    if (OC_1and2_period > 0) {
        OC1R = 0;
        OC1RS = 0;
        OC1CON = 0;
        OC2R = 0;
        OC2RS = 0;
        OC2CON = 0;
        T2CON = 0;
        PR2 = 0xFFFF;
        if (OC_1and2_period < 1600.0) {
            T2CKPS = 1;
            PR2 = ((OC_1and2_period * micro * Fcyc) / (1));
            T2CONbits.TCKPS = 0;
        } else if (OC_1and2_period < 13000.0) {
            T2CKPS = 8;
            PR2 = ((OC_1and2_period * micro * Fcyc) / (8));
            T2CONbits.TCKPS = 1;
        } else if (OC_1and2_period < 100000.0) {
            T2CKPS = 64;
            PR2 = ((OC_1and2_period * micro * Fcyc) / (64));
            T2CONbits.TCKPS = 2;
        } else if (OC_1and2_period < 838848.0) {
            T2CKPS = 256;
            PR2 = ((OC_1and2_period * micro * Fcyc) / (256));
            T2CONbits.TCKPS = 3;
        } else {
            T2CKPS = 256;
            PR2 = 0xFFFF;
            T2CONbits.TCKPS = 3;
        }
        //PR2 = (((OC_1and2_period) * (micro) * (Fcyc)) / (T2CKPS));
        if (OC1_PIN != not_used_module) {

            PMD2bits.OC1MD = 0;
            OC1CONbits.OCM = 6;
            OC1CONbits.OCTSEL = 0;
            T2CONbits.TON = 1;
        }

        if (OC2_PIN != not_used_module) {

            PMD2bits.OC2MD = 0;
            OC2CONbits.OCM = 6;
            OC2CONbits.OCTSEL = 0;
            T2CONbits.TON = 1;
        }

    }
    if (OC_3and4_period > 0) {
        OC3R = 0;
        OC3RS = 0;
        OC3CON = 0;
        OC4R = 0;
        OC4RS = 0;
        OC4CON = 0;
        T3CON = 0;
        if (OC_3and4_period < 1600) {
            T3CKPS = 1;
            PR2 = ((OC_3and4_period * micro * Fcyc) / (1));
            T3CONbits.TCKPS = 0;
        } else if (OC_3and4_period < 13000) {
            T3CKPS = 8;
            PR3 = ((OC_3and4_period * micro * Fcyc) / (8));
            T3CONbits.TCKPS = 1;
        } else if (OC_3and4_period < 100000) {
            T3CKPS = 64;
            PR3 = ((OC_3and4_period * micro * Fcyc) / (64));
            T3CONbits.TCKPS = 2;
        } else if (OC_3and4_period < 838848) {
            T3CKPS = 256;
            PR3 = ((OC_3and4_period * micro * Fcyc) / (256));
            T3CONbits.TCKPS = 3;
        } else {
            T3CKPS = 256;
            PR3 = 0xFFFF;
            T3CONbits.TCKPS = 3;
        }
        PR3 = (((OC_3and4_period) * (micro) * (Fcyc)) / (T3CKPS));
        if (OC3_PIN != not_used_module) {

            PMD2bits.OC3MD = 0;
            OC3CONbits.OCM = 6;
            OC3CONbits.OCTSEL = 1; //1:TMR3をタイマーベースとする
            T3CONbits.TON = 1;
        }
        if (OC4_PIN != not_used_module) {

            PMD2bits.OC3MD = 0;
            OC4CONbits.OCM = 6;
            OC4CONbits.OCTSEL = 1;
            T3CONbits.TON = 1;
        }

    }

    return;
}

void setduty_OC(uint8_t Move_Number_of_OC, bool pinfunction, uint16_t duty) {


    /*if (duty > PR2) {
        duty = PR2;
    }*/

    if (Move_Number_of_OC == 1) {
        if (pinfunction == active_HI) {
            OC1RS = ((uint16_t) abs(duty));
        } else {
            OC1RS = PR2 - ((uint16_t) abs(duty));
        }
    } else if (Move_Number_of_OC == 2) {
        if (pinfunction == active_HI) {
            OC2RS = ((uint16_t) abs(duty));
        } else {
            OC2RS = PR2 - ((uint16_t) abs(duty));
        }
    } else if (Move_Number_of_OC == 3) {
        if (pinfunction == active_HI) {
            OC3RS = ((uint16_t) fabs(duty));
        } else {
            OC3RS = PR3 - ((uint16_t) fabs(duty));
        }
    } else {
        if (pinfunction == active_HI) {
            OC4RS = ((uint16_t) fabs(duty));
        } else {
            OC4RS = PR3 - ((uint16_t) fabs(duty));
        }
    }

    return;

}

void setdutySec_for_OC(unsigned char Move_Number_of_OC, bool pinfunction, unsigned short MicroSec) {

    if (Move_Number_of_OC == 1) {
        if (pinfunction == true) {
            OC1RS = ((MicroSec * micro * Fcyc) / (T2CKPS));
        } else {
            OC1RS = PR2 - ((MicroSec * micro * Fcyc) / (T2CKPS));
        }
    } else if (Move_Number_of_OC == 2) {
        if (pinfunction == true) {
            OC2RS = ((MicroSec * micro * Fcyc) / (T2CKPS));
        } else {
            OC2RS = PR2 - ((MicroSec * micro * Fcyc) / (T2CKPS));
        }
    } else if (Move_Number_of_OC == 3) {
        if (pinfunction == true) {
            OC3RS = ((MicroSec * micro * Fcyc) / (T3CKPS));
        } else {
            OC3RS = PR3 - ((MicroSec * micro * Fcyc) / (T3CKPS));
        }
    } else {
        if (pinfunction == true) {
            OC4RS = ((MicroSec * micro * Fcyc) / (T3CKPS));
        } else {
            OC4RS = PR3 - ((MicroSec * micro * Fcyc) / (T3CKPS));
        }
    }

    return;
}

void setupTMR(void) {

    T1CON = 0;
    TMR1 = 0;
    PR1 = 0xFFFF;
    T2CON = 0;
    TMR2 = 0;
    PR2 = 0xFFFF;
    T3CON = 0;
    TMR3 = 0;
    PR3 = 0xFFFF;
    T4CON = 0;
    TMR4 = 0;
    PR4 = 0xFFFF;
    T5CON = 0;
    TMR5 = 0;
    PR5 = 0xFFFF;

    if (TMR1_IP != not_used_module) {
        if (TMR1_IP == 0) {
            IEC0bits.T1IE = 0;
            IPC0bits.T1IP = 0;
            IFS0bits.T1IF = 0;
        } else if (TMR1_IP < 7) {
            IEC0bits.T1IE = 1;
            IPC0bits.T1IP = TMR1_IP;
            IFS0bits.T1IF = 0;
        } else {
            IEC0bits.T1IE = 1;
            IPC0bits.T1IP = 7;
            IFS0bits.T1IF = 0;
        }
    }

    if (TMR2_IP != not_used_module) {
        if (TMR2_IP == 0) {
            IEC0bits.T2IE = 0;
            IPC1bits.T2IP = 0;
            IFS0bits.T2IF = 0;
        } else if (TMR2_IP < 7) {
            IEC0bits.T2IE = 1;
            IPC1bits.T2IP = TMR2_IP;
            IFS0bits.T2IF = 0;
        } else {
            IEC0bits.T2IE = 1;
            IPC1bits.T2IP = 7;
            IFS0bits.T2IF = 0;
        }
    }

    if (TMR3_IP != not_used_module) {
        if (TMR3_IP == 0) {
            IEC0bits.T3IE = 0;
            IPC2bits.T3IP = 0;
            IFS0bits.T3IF = 0;
        } else if (TMR3_IP < 7) {
            IEC0bits.T3IE = 1;
            IPC2bits.T3IP = TMR3_IP;
            IFS0bits.T3IF = 0;
        } else {
            IEC0bits.T3IE = 1;
            IPC2bits.T3IP = 7;
            IFS0bits.T3IF = 0;
        }
    }

    if (TMR4_IP != not_used_module) {
        if (TMR4_IP == 0) {
            IEC1bits.T4IE = 0;
            IPC6bits.T4IP = 0;
            IFS1bits.T4IF = 0;
        } else if (TMR4_IP < 7) {
            IEC1bits.T4IE = 1;
            IPC6bits.T4IP = TMR4_IP;
            IFS1bits.T4IF = 0;
        } else {
            IEC1bits.T4IE = 1;
            IPC6bits.T4IP = 7;
            IFS1bits.T4IF = 0;
        }
    }

    if (TMR5_IP != not_used_module) {
        if (TMR5_IP == 0) {
            IEC1bits.T5IE = 0;
            IPC7bits.T5IP = 0;
            IFS1bits.T5IF = 0;
        } else if (TMR5_IP < 7) {
            IEC1bits.T5IE = 1;
            IPC7bits.T5IP = TMR5_IP;
            IFS1bits.T5IF = 0;
        } else {
            IEC1bits.T5IE = 1;
            IPC7bits.T5IP = 7;
            IFS1bits.T5IF = 0;
        }
    }

    return;
}

void setdelay(unsigned char move_nomber_of_TMR, unsigned char mode, float Sec) {

    float TMR;
    if (mode == 0) {
        TMR = ((Sec * milli * Fcyc) / (256.0));
        //TMR = ((Sec * milli) / ((float)256.0 * (float)Fcyc));
    } else if (mode == 1) {
        TMR = 0xFFFF - ((Sec * micro * FCY) / (8.0));
    } else if (mode == 2) {
        TMR = 0xFFFF - (Sec * nano * FCY);
    } else {
        TMR = 0;
    }

    if (move_nomber_of_TMR == 1) {
        if (mode == 0) {
            T1CONbits.TCKPS = 3;
        } else if (mode == 1) {
            T1CONbits.TCKPS = 1;
        } else if (mode == 2) {
            T1CONbits.TCKPS = 0;
        }
        TMR1 = 0;
        PR1 = TMR;
        IFS0bits.T1IF = 0;
        T1CONbits.TON = 1;
        return;
    } else if (move_nomber_of_TMR == 2) {
        if (mode == 0) {
            T2CONbits.TCKPS = 3;
        } else if (mode == 1) {
            T2CONbits.TCKPS = 1;
        } else if (mode == 2) {
            T2CONbits.TCKPS = 0;
        }
        TMR2 = TMR;
        IFS0bits.T2IF = 0;
        T2CONbits.TON = 1;
        return;
    } else if (move_nomber_of_TMR == 3) {
        if (mode == 0) {
            T3CONbits.TCKPS = 3;
        } else if (mode == 1) {
            T3CONbits.TCKPS = 1;
        } else if (mode == 2) {
            T3CONbits.TCKPS = 0;
        }
        TMR3 = TMR;
        IFS0bits.T3IF = 0;
        T3CONbits.TON = 1;
        return;
    } else if (move_nomber_of_TMR == 4) {
        if (mode == 0) {
            T4CONbits.TCKPS = 3;
        } else if (mode == 1) {
            T4CONbits.TCKPS = 1;
        } else if (mode == 2) {
            T4CONbits.TCKPS = 0;
        }
        TMR4 = TMR;
        IFS1bits.T4IF = 0;
        T4CONbits.TON = 1;
        return;
    } else if (move_nomber_of_TMR == 5) {
        if (mode == 0) {
            T5CONbits.TCKPS = 3;
        } else if (mode == 1) {
            T5CONbits.TCKPS = 1;
        } else if (mode == 2) {
            T5CONbits.TCKPS = 0;
        }
        TMR5 = TMR;
        IFS1bits.T5IF = 0;
        T5CONbits.TON = 1;
        return;
    }
    return;
}

void setupQEI(void) {

    if ((QEI1_PIN_A != not_used_module) && (QEI1_PIN_B != not_used_module)) {

        if (QEI1_IP != not_used_module) {
            if (QEI1_IP == 0) {
                IEC3bits.QEI1IE = 0;
                IPC14bits.QEI1IP = 0;
                IFS3bits.QEI1IF = 0;
            } else if (QEI1_IP < 7) {
                IEC3bits.QEI1IE = 1;
                IPC14bits.QEI1IP = QEI1_IP;
                IFS3bits.QEI1IF = 0;
            } else {
                IEC3bits.QEI1IE = 1;
                IPC14bits.QEI1IP = 7;
                IFS3bits.QEI1IF = 0;
            }
        }//割り込み設定

        POS1CNT = 0x7FFF;
        MAX1CNT = 0xFFFF; // +32767 ~ -32767, +32767のときリセット
        QEI1CON = 0;
        PMD1bits.QEI1MD = 0;
        if (QEI1_type == speed_control) {
            QEI1CONbits.QEIM = 7;
        } else {
            QEI1CONbits.QEIM = 4;
        }
        QEI1CONbits.PCDOUT = 0;


    }
    if ((QEI2_PIN_A != not_used_module) && (QEI2_PIN_B != not_used_module)) {

        if (QEI2_IP != not_used_module) {
            if (QEI2_IP == 0) {
                IEC4bits.QEI2IE = 0;
                IPC18bits.QEI2IP = 0;
                IFS4bits.QEI2IF = 0;
            } else if (QEI2_IP < 7) {
                IEC4bits.QEI2IE = 1;
                IPC18bits.QEI2IP = QEI1_IP;
                IFS4bits.QEI2IF = 0;
            } else {
                IEC4bits.QEI2IE = 1;
                IPC18bits.QEI2IP = 7;
                IFS4bits.QEI2IF = 0;
            }
        }//割り込み設定

        POS2CNT = 0x7FFF;
        MAX2CNT = 0xFFFF; // +32767 ~ -32767, +32767のときリセット
        QEI2CON = 0;
        PMD3bits.QEI2MD = 0;
        if (QEI2_type == speed_control) {
            QEI2CONbits.QEIM = 7;
        } else {
            QEI2CONbits.QEIM = 4;
        }
        QEI2CONbits.PCDOUT = 0;

    }

    return;
}

bool Controler_checker_ISR(uint8_t using_UART, uint8_t* datas) {

    static Controler_status status;

    // ****************************   データ要素確認   ******************************
    if (status.recievetime == 17) {

        if ((datas[0] == datas[1]) && (datas[0] == 'S') && (datas[2] == (datas[3] ^ 0xFF)) && (datas[4] == (datas[5] ^ 0xFF))&&
                (datas[6] == (datas[7] ^ 0xFF))&&(datas[8] == (datas[9] ^ 0xFF))&&(datas[10] == (datas[11] ^ 0xFF))&&
                (datas[12] == (datas[13] ^ 0xFF))&&(datas[14] == (datas[15] ^ 0xFF))&&
                ((datas[16] == datas[17]) && (datas[16] == 'E'))) {

            status.useful = 1; //データが有用かどうか確認

        }

        if (status.useful == true) {
            status.WasteRecieve = OFF; //無駄にデータの受信をしない
        } else {
            status.WasteRecieve = ON; //１度無駄にデータを受信する
        }
        status.FirstRXIF = ON; //データは１番目の有効なデータである
        status.recievetime = 0; //リングバッファ処理
    }
    // ****************   受信動作   ************************************************
    if ((status.WasteRecieve == ON) && (status.recievetime == 0)) {
        status.WasteRecieve = OFF;
    } else if ((status.WasteRecieve == OFF) && (status.FirstRXIF == ON) && (status.recievetime == 0)) {
        status.FirstRXIF = OFF;
    } else {
        status.recievetime++;
    }

    if (using_UART == 1) {
        datas[status.recievetime] = U1RXREG;
        _U1RXIF = 0;
    } else if (using_UART == 2) {
        datas[status.recievetime] = U2RXREG;
        _U2RXIF = 0;
    }


    return status.useful;
}

unsigned char makeASCII(unsigned char result) {

    if (result <= 9) {
        result = result + 48;
    } else {
        result = result + 55;
    }

    return result;

}

void makeASCIIs(unsigned const char mode, int16_t result, uint8_t* returns) {

    if (result > 0) {
        returns[0] = 43;
    } else {
        returns[0] = 45;
    }

    result = abs(result);

    if (mode == 16) {
        returns[4] = result & 0x000F;
        returns[4] = makeASCII(returns[4]);

        returns[3] = (result >> 4)& 0x000F;
        returns[3] = makeASCII(returns[3]);

        returns[2] = (result >> 8)& 0x000F;
        returns[2] = makeASCII(returns[2]);

        returns[1] = (result >> 12)& 0x000F;
        returns[1] = makeASCII(returns[1]);

        returns[5] = 32; //スペース

    } else if (mode == 10) {
        returns[1] = (result / 10000);
        returns[2] = (result / 1000)-(returns[1]*10);
        returns[3] = (result / 100)-(returns[2]*10 + returns[1]*100);
        returns[4] = (result / 10)-(returns[3]*10 + returns[2]*100 + returns[1]*1000);
        returns[5] = result - (returns[4]*10 + returns[3]*100 + returns[2]*1000 + returns[1]*10000);

        returns[1] = makeASCII(returns[1]);
        returns[2] = makeASCII(returns[2]);
        returns[3] = makeASCII(returns[3]);
        returns[4] = makeASCII(returns[4]);
        returns[5] = makeASCII(returns[5]);

    }

    returns[6] = 32; //スペース

    return;
}

float QEICNT1 = 0;

#if 0

void tmr_ISR_PID_Control(PID_variables* elements) {

    /*固定小数点の使用やPIDの計算式の改良をするべき*/

    static bool times = 0;

    static float ErrorA1 = 0.0, ErrorA2 = 0.0, ErrorA_total = 0.0, Error_differential = 0.0;
    static float Dvariable_A = 0.0, now_variable_A = 0.0; //単位のない数値


    //static float ErrorB1 = 0.0, ErrorB2 = 0.0, ErrorB3 = 0.0;


#if 0
    /*Drpmの上昇*/
    if (((*elements->DrpmA)>(*elements->now_DrpmA)) && (labs(*elements->DrpmA - *elements->now_DrpmA)>(*elements->DrpmA_up_speed))) {
        *elements->now_DrpmA = ((*elements->now_DrpmA)+(((float) *elements->DrpmA_up_speed) * (QEI_sampling_Sec)));
    } else if (((*elements->DrpmA)<(*elements->now_DrpmA)) && (labs(*elements->DrpmA - *elements->now_DrpmA)>(*elements->DrpmA_up_speed))) {
        *elements->now_DrpmA = ((*elements->now_DrpmA)+((((float) *elements->DrpmA_up_speed) * (-1.0)) * (QEI_sampling_Sec)));
    } else {
        *elements->now_DrpmA = *elements->DrpmA;
    }

    if (((*elements->DrpmB)>(*elements->now_DrpmB)) && (labs(*elements->DrpmB - *elements->now_DrpmB)>(*elements->DrpmB_up_speed))) {
        *elements->now_DrpmB = ((*elements->now_DrpmB)+(((float) *elements->DrpmB_up_speed) * (QEI_sampling_Sec)));
    } else if (((*elements->DrpmB)<(*elements->now_DrpmB)) && (labs(*elements->DrpmB - *elements->now_DrpmB)>(*elements->DrpmB_up_speed))) {
        *elements->now_DrpmB = ((*elements->now_DrpmB)+((((float) *elements->DrpmB_up_speed) * (-1.0)) * (QEI_sampling_Sec)));
    } else {
        *elements->now_DrpmB = *elements->DrpmB;
    }
#endif
    /*PIDの係数計算*/

    /*  モータAのPID制御    */

    if (Dvariable_A != ((*(elements->now_DrpmA)) * QEI1_pulse_per_revolution)) {
        Dvariable_A = (*(elements->now_DrpmA)) * QEI1_pulse_per_revolution;
        ErrorA_total = 0.0;
        ErrorA1 = 0.0;
    }
    now_variable_A = ((int16_t) (POS1CNT - 0x7FFF));
    *(elements->now_rpmA1) = (((now_variable_A) * (QEI1_revolution_per_pulse)) / (QEI_sampling_Minute)); //RPMの計算

    ErrorA2 = ErrorA1; //前回偏差の代入
    ErrorA1 = (Dvariable_A - now_variable_A); //今回の偏差
    ErrorA_total = ErrorA_total + ((ErrorA1) * (QEI_sampling_Minute)); //積算誤差
    Error_differential = (ErrorA1 - ErrorA2) / (QEI_sampling_Minute); //微分

    /*モータBのPID制御*/
#if 0
    rpm_B2 = *elements->now_rpmB1;
    *elements->now_rpmB1 = (int16_t) (((int16_t) (POS2CNT)) / ((QEI_sampling_Minute)*(QEI2_pulse_per_revolution)));

    DPB = ((*elements->P_B_gein) * ((float) (*elements->now_DrpmB - *elements->now_rpmB1)));
    DIB += ((*elements->I_B_gein) * ((float) (*elements->now_DrpmB - *elements->now_rpmB1)));
    DDB = ((*elements->D_B_gein) * (((float) (*elements->now_DrpmB - *elements->now_rpmB1)) - Last_errorB));
    Last_errorB = ((float) (*elements->now_DrpmB - *elements->now_rpmB1));
#endif
    /*dutyの算出*/
    *(elements->now_dutyA) = ((DPA_PID) + (DIA_PID) + (DDA_PID)); //PIDの計算
    //*(elements->now_dutyA) = (DPA_PID);

    /*上限値の設定*/
    if (((*(elements->now_dutyA)) > (0.0)) && ((*(elements->now_dutyA)) > ((float) PR2))) {
        *(elements->now_dutyA) = ((float) PR2);
    } else if (((*(elements->now_dutyA)) < (0.0)) && ((*(elements->now_dutyA) * (-1.0)) > ((float) PR2))) {
        *(elements->now_dutyA) = (((float) PR2) * (-1.0));
    }


    setduty_OC(OC_for_MOTER_A, active_LOW, (uint16_t) fabs((*(elements->now_dutyA)))); //PWMの設定

#if 0
    *elements->now_dutyB = DPB + DIB + DDB;

    if ((fabs(*elements->now_dutyB)) >= ((float) PR2)) {
        if (*elements->now_dutyB > 0) {
            *elements->now_dutyB = ((float) (PR2));
        } else {
            *elements->now_dutyB = ((((float) PR2) * (-1.0)));
        }
    }
    setduty_OC(OC_for_MOTER_B, active_LOW, *elements->now_dutyB);
#endif

    /*LATの制御*/
    if ((*(elements->now_dutyA)) == 0.0) {
        SET_BRAKE_A(); //ブレーキ
    } else if ((*(elements->now_dutyA)) > 0.0) {
        SET_TRUN_NORMAL_A(); //正転
    } else {
        SET_TRUN_REVERSE_A(); //逆転
    }

#if 0
    if ((*(elements->now_dutyB) == 0) || (*elements->now_rpmB1 == 0)) {//考えるべきとこ
        SET_BRAKE_B(); //ブレーキ
    } else if (*elements->now_dutyB > 0) {
        SET_TRUN_NORMAL_B(); //正転
    } else {
        SET_TRUN_REVERSE_B(); //逆転
    }
#endif

    POS1CNT = 0x7FFF; //中央値
    //POS2CNT = 0;
    return;

}
#endif

#define reset_Position_Variable_A(){\
ErrorA1 = 0.0;ErrorA2 = 0.0;ErrorA_total = 0.0;Error_differential_A = 0.0;\
}

#define reset_Position_Variable_B(){\
ErrorB1 = 0.0;ErrorB2 = 0.0;ErrorB_total = 0.0;Error_differential_B = 0.0;\
}

#define Amount_of_change_Dposition_A 1.0
#define Amount_of_change_Dposition_B 1.0

void tmr_ISR_Position_Control(Position_variables* elements) {

    /*固定小数点の使用やPIDの計算式の改良をするべき*/

    static float ErrorA1 = 0.0, ErrorA2 = 0.0, ErrorA_total = 0.0, Error_differential_A = 0.0;
    static float ErrorB1 = 0.0, ErrorB2 = 0.0, ErrorB_total = 0.0, Error_differential_B = 0.0;

    static float Dvariable_A = 0.0, now_variable_A = 0.0;
    static float Dvariable_B = 0.0, now_variable_B = 0.0;

    if (*(elements->now_positionA) == 0.0) {
        reset_Position_Variable_A(); //各変数の初期化
    }
    if (*(elements->now_positionB) == 0.0) {
        reset_Position_Variable_B(); //各変数の初期化
    }

    /* 目標値の変更　*/
    if (((((*(elements->now_DpositionA) - *(elements->now_positionA)) > (Amount_of_change_Dposition_A * 0.95)) &&
            ((*(elements->now_DpositionA) - *(elements->now_positionA)) < (Amount_of_change_Dposition_A * 1.05)))
            || ((*(elements->now_positionA) == 0.0)))
            && (*(elements->now_DpositionA) != *(elements->DpositionA))) {

        if (*(elements->DpositionA) > 0.0) {
            *(elements->now_DpositionA) = *(elements->now_DpositionA) + Amount_of_change_Dposition_A;
        } else {
            *(elements->now_DpositionA) = *(elements->now_DpositionA) - Amount_of_change_Dposition_A;
        }

        if (fabs(*(elements->now_DpositionA)) > fabs(*(elements->DpositionA))) {
            *(elements->now_DpositionA) = *(elements->DpositionA);
        }

    }//誤差5％以内のとき

    if (((((*(elements->now_DpositionB) - *(elements->now_positionB)) > (Amount_of_change_Dposition_B * 0.95)) &&
            ((*(elements->now_DpositionB) - *(elements->now_positionB)) < (Amount_of_change_Dposition_B * 1.05)))
            || ((*(elements->now_positionB) == 0.0)))
            && (*(elements->now_DpositionB) != *(elements->DpositionB))) {

        if (*(elements->DpositionB) > 0.0) {
            *(elements->now_DpositionB) = *(elements->now_DpositionB) + Amount_of_change_Dposition_B;
        } else {
            *(elements->now_DpositionB) = *(elements->now_DpositionB) - Amount_of_change_Dposition_B;
        }

        if (fabs(*(elements->now_DpositionB)) > fabs(*(elements->DpositionB))) {
            *(elements->now_DpositionB) = *(elements->DpositionB);
        }

    }//誤差5％以内のとき


    now_variable_A = (*(elements->now_positionA)) * QEI1_pulse_per_revolution;
    now_variable_B = (*(elements->now_positionB)) * QEI2_pulse_per_revolution;
    Dvariable_A = (*(elements->now_DpositionA)) * QEI1_pulse_per_revolution;
    Dvariable_B = (*(elements->DpositionB)) * QEI2_pulse_per_revolution;

    /*PIDの係数計算*/
    /*  モータAのPID制御    */

    now_variable_A = now_variable_A + ((int16_t) (POS1CNT - 0x7FFF));
    *(elements->now_positionA) = now_variable_A * QEI1_revolution_per_pulse; //進んだ回転数の計算

    ErrorA2 = ErrorA1; //前回偏差の代入
    ErrorA1 = Dvariable_A - now_variable_A; //今回の偏差
    ErrorA_total += ErrorA1 * (QEI_sampling_Minute); //積算誤差
    Error_differential_A = (ErrorA1 - ErrorA2) / (QEI_sampling_Minute); //微分

    /*モータBのPID制御*/

    now_variable_B = now_variable_B + ((int16_t) (POS2CNT - 0x7FFF));
    *(elements->now_positionB) = now_variable_B * QEI2_revolution_per_pulse; //進んだ回転数の計算

    ErrorB2 = ErrorB1; //前回偏差の代入
    ErrorB1 = Dvariable_B - now_variable_B; //今回の偏差
    ErrorB_total += ErrorB1 * (QEI_sampling_Minute); //積算誤差
    Error_differential_B = (ErrorB1 - ErrorB2) / (QEI_sampling_Minute); //微分

    /*dutyの算出*/
    *(elements->dutyA) = ((DPA_PID) + (DIA_PID) + (DDA_PID)); //PIDの計算
    //*(elements->dutyB) = ((DPB_PID) + (DIB_PID) + (DDB_PID)); //PIDの計算
    *(elements->dutyB) = (DPB_PID);

    /*上限値の設定*/
    if (((*(elements->dutyA)) > (0.0)) && ((*(elements->dutyA)) > ((float) PR2))) {
        *(elements->dutyA) = ((float) PR2);
    } else if (((*(elements->dutyA)) < (0.0)) && ((*(elements->dutyA) * (-1.0)) > ((float) PR2))) {
        *(elements->dutyA) = (((float) PR2) * (-1.0));
    }
    if (((*(elements->dutyB)) > (0.0)) && ((*(elements->dutyB)) > ((float) PR2))) {
        *(elements->dutyB) = ((float) PR2);
    } else if (((*(elements->dutyB)) < (0.0)) && ((*(elements->dutyB) * (-1.0)) > ((float) PR2))) {
        *(elements->dutyB) = (((float) PR2) * (-1.0));
    }


    setduty_OC(OC_for_MOTER_A, active_LOW, (uint16_t) fabs((*(elements->dutyA)))); //PWMの設定
    setduty_OC(OC_for_MOTER_B, active_LOW, (uint16_t) fabs((*(elements->dutyB)))); //PWMの設定


    /*LATの制御*/
    if ((*(elements->dutyA)) == 0.0) {
        SET_BRAKE_A(); //ブレーキ
    } else if ((*(elements->dutyA)) > 0.0) {
        SET_TRUN_NORMAL_A(); //正転
    } else {
        SET_TRUN_REVERSE_A(); //逆転
    }

    if ((*(elements->dutyB)) == 0.0) {
        SET_BRAKE_B(); //ブレーキ
    } else if ((*(elements->dutyB)) > 0.0) {
        SET_TRUN_NORMAL_B(); //正転
    } else {
        SET_TRUN_REVERSE_B(); //逆転
    }

    POS1CNT = 0x7FFF; //中央値
    POS2CNT = 0x7FFF;
    return;

}






