/*
 * File:   main.c
 * Author: Shuuya
 *
 * Created on 2016/03/16, 20:00
 */

#define FCY (20000000.0)
#include <stdio.h>
#include <stdlib.h>
#include <xc.h>
#include <stdint.h>
#include <float.h>


    // FBS
#pragma config BWRP = WRPROTECT_OFF     // Boot Segment Write Protect (Boot Segment may be written)
#pragma config BSS = NO_FLASH           // Boot Segment Program Flash Code Protection (No Boot program Flash segment)
#pragma config RBS = NO_RAM             // Boot Segment RAM Protection (No Boot RAM)

// FSS
#pragma config SWRP = WRPROTECT_OFF     // Secure Segment Program Write Protect (Secure segment may be written)
#pragma config SSS = NO_FLASH           // Secure Segment Program Flash Code Protection (No Secure Segment)
#pragma config RSS = NO_RAM             // Secure Segment Data RAM Protection (No Secure RAM)

// FGS
#pragma config GWRP = OFF               // General Code Segment Write Protect (User program memory is not write-protected)
#pragma config GSS = OFF                // General Segment Code Protection (User program memory is not code-protected)

// FOSCSEL
#pragma config FNOSC = FRC              // Oscillator Mode (Internal Fast RC (FRC))
#pragma config IESO = ON                // Internal External Switch Over Mode (Start-up device with FRC, then automatically switch to user-selected oscillator source when ready)

// FOSC
#pragma config POSCMD = NONE            // Primary Oscillator Source (Primary Oscillator Disabled)
#pragma config OSCIOFNC = ON            // OSC2 Pin Function (OSC2 pin has digital I/O function)
#pragma config IOL1WAY = OFF            // Peripheral Pin Select Configuration (Allow Multiple Re-configurations)
#pragma config FCKSM = CSECMD           // Clock Switching and Monitor (Clock switching is enabled, Fail-Safe Clock Monitor is disabled)

// FWDT
#pragma config WDTPOST = PS32768        // Watchdog Timer Postscaler (1:32,768)
#pragma config WDTPRE = PR128           // WDT Prescaler (1:128)
#pragma config WINDIS = OFF             // Watchdog Timer Window (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = OFF             // Watchdog Timer Enable (Watchdog timer enabled/disabled by user software)

// FPOR
#pragma config FPWRT = PWR128           // POR Timer Value (128ms)
#pragma config ALTI2C = OFF             // Alternate I2C  pins (I2C mapped to SDA1/SCL1 pins)
#pragma config LPOL = OFF               // Motor Control PWM Low Side Polarity bit (PWM module low side output pins have active-low output polarity)
#pragma config HPOL = OFF               // Motor Control PWM High Side Polarity bit (PWM module high side output pins have active-low output polarity)
#pragma config PWMPIN = ON             // Motor Control PWM Module Pin Mode bit (PWM module pins controlled by PWM module at device Reset)

// FICD
#pragma config ICS = PGD1               // Comm Channel Select (Communicate on PGC1/EMUC1 and PGD1/EMUD1)
#pragma config JTAGEN = OFF             // JTAG Port Enable (JTAG is Disabled)


#include "RS422S.h"

void __attribute__((interrupt, auto_psv)) _U1RXInterrupt(void);
void __attribute__((interrupt, auto_psv)) _U1TXInterrupt(void);
void __attribute__((interrupt, auto_psv)) _T1Interrupt(void);

void test_set_RS422S();
uint8_t before_select = 0xFF;

int main(void) {

    setup_dsPIC();

    TRISA = 0x0F;
    number_of_valid_data_S = 2;
    device_id_S = (PORTA & 0x0F);
    setupTMR();
    setupOC();
    setupQEI();
    setdelay(1, 0, QEI_sampling_MilliSec);
    setupUART();

    while (1) {
        if (STAP_mode_select_RS422S == PWM_CONTROLLER) {
            if (before_select != PWM_CONTROLLER) {
                init_PWM_RS422S();
                before_select = PWM_CONTROLLER;
            }

            PWM_generater_RS422();
            if ((dutyA_percent_RS422S) == 0.0) {
                SET_BRAKE_A();//�u���[�L
            } else if ((dutyA_percent_RS422S) > 0.0) {
                SET_TRUN_NORMAL_A();//���]
            } else {
                SET_TRUN_REVERSE_A();//�t�]
            }

            if ((dutyB_percent_RS422S) == 0.0) {
                SET_BRAKE_B();//�u���[�L
            } else if ((dutyB_percent_RS422S) > 0.0) {
                SET_TRUN_NORMAL_B(); //���]
            } else {
                SET_TRUN_REVERSE_B(); //�t�]
            }

        } else if (STAP_mode_select_RS422S == PID_CONTROLLER) {
            if (before_select != PID_CONTROLLER) {
                init_PID_RS422S();
                before_select = PID_CONTROLLER;
            }
        } else if (STAP_mode_select_RS422S == POSITION_CONTROLLER) {
            if (before_select != POSITION_CONTROLLER) {
                init_Position_RS422S();
                before_select = POSITION_CONTROLLER;
            }
        }
    }

}

void test_set_RS422S() {
    STAP_mode_select_RS422S = POSITION_CONTROLLER;
    
    P_A_gein_RS422S = 30.0;
    I_A_gein_RS422S = 0.01;
    D_A_gein_RS422S = 0.0;
    Dposition_A_RS422S      = 0.3;
    
    P_B_gein_RS422S = 10.0;
    I_B_gein_RS422S = 0.0;
    D_B_gein_RS422S = 0.0;
    Dposition_B_RS422S      = 5.0;
    
}

void __attribute__((interrupt, auto_psv)) _T1Interrupt(void) {

    _T1IF = 0;
    device_id_S = (PORTA & 0x0F);
    setdelay(1, 0, QEI_sampling_MilliSec);

    if (STAP_mode_select_RS422S == PID_CONTROLLER) {
        //tmr_ISR_PID_Control_RS422();
    } else if (STAP_mode_select_RS422S == POSITION_CONTROLLER) {
        tmr_ISR_Position_Control_RS422();
    }

    return;

}

void __attribute__((interrupt, auto_psv)) _U1RXInterrupt(void) {

    _U1RXIF = 0;
    rx_ISR_RS422();
    return;
}

void __attribute__((interrupt, auto_psv)) _U1TXInterrupt(void) {

    _U1TXIF = 0;
    tx_ISR_RS422();
    return;

}

